# PPK-Praktikum 2 : XML dan JSON

## Identitas

```
Nama : Novanni Indi Pradana
NIM  : 222011436
Kelas: 3SI3

```

## Deskripsi

Extensible Markup Language (XML) adalah bahasa markup yang diciptakan oleh konsorsium World Wide Web (W3C). Bahasa ini berfungsi untuk menyederhanakan proses penyimpanan dan pengiriman data antarserver. Proses penyimpanan dan pengiriman data ini perlu disederhanakan karena setiap server mungkin memiliki sistem atau platform yang berbeda-beda sehingga pertukaran data antar server mungkin akan memakan banyak waktu jika tidak menggunakan standar tertentu. XML merupakan salah satu standar untuk pertukaran data ini. XML menyimpan data dalam format teks yang sederhana. Sehingga tidak perlu mengubah format data sama sekali. Bahkan format datanya pun akan tetap sama walaupun ditransfer ke server lain. Oleh karena itu, XML juga mudah untuk diperbarui ke operating system baru atau browser baru. Bahasanya pun mudah untuk dibaca manusia, komputer, hingga teknologi pengenal suara. 

Sedangkan JSON (JavaScript Object Notation) juga merupakan alat untuk pertukaran data, menyimpan dan mentransfer data seperti XML. Namun berbeda dengan XML dan format lainnya yang memiliki fungsi serupa, JSON memiliki struktur data yang sederhana dan mudah dipahami. Itulah mengapa JSON sering digunakan pada API. JSON sendiri terdiri dari dua struktur, yaitu:  Kumpulan value yang saling berpasangan. Dalam JSON, contohnya adalah object dan Daftar value yang berurutan, seperti array. 


## Kegiatan Praktikum

## 1. Buatlah laporan tangkapan layar pada kegiatan yang anda lakukan pada 2.4.

![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-2/-/raw/main/screenshot/Screenshot%20(402).png)

![note.xml](https://gitlab.com/PradanaIN/ppk-praktikum-2/-/raw/main/screenshot/Screenshot%20(403).png)

![simple.xml](https://gitlab.com/PradanaIN/ppk-praktikum-2/-/raw/main/screenshot/Screenshot%20(404).png)

## 2. Untuk melakukan validasi xml anda dapat menggunakan tools online seperti pada https://www.xmlvalidation.com . Bereksperimenlah dengan XML, DTD, dan Schema anda sendiri.

![produk.xml](https://gitlab.com/PradanaIN/ppk-praktikum-2/-/raw/main/screenshot/Screenshot%20(405).png)

![produk.xml validation](https://gitlab.com/PradanaIN/ppk-praktikum-2/-/raw/main/screenshot/Screenshot%20(407).png)

![pesan.xml](https://gitlab.com/PradanaIN/ppk-praktikum-2/-/raw/main/screenshot/Screenshot%20(406).png)

![pesan.xml validation](https://gitlab.com/PradanaIN/ppk-praktikum-2/-/raw/main/screenshot/Screenshot%20(408).png)
